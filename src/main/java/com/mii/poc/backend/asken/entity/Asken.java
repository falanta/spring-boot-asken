package com.mii.poc.backend.asken.entity;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "asken_trx")
public class Asken implements Serializable {
private static final long serialVersionUID = -2343243243242432341L;
@Id @GeneratedValue(generator="system-uuid")
@GenericGenerator(name="system-uuid", strategy = "uuid")
private String id;
@Column(name = "nama_lengkap")
private String nama_lengkap;
@Column(name = "no_hp")
private String no_hp;
@Column(name = "email")
private String email;
@Column(name = "no_plat")
private String no_plat;
@Column(name = "no_mesin")
private String no_mesin;
@Column(name = "no_rangka")
private String no_rangka;
@Column(name = "sumber_rekening")
private String sumber_rekening;
@Column(name = "produk")
private String produk;
@Column(name = "periode_asuransi")
private String periode_asuransi;
@Column(name = "tahun_kendaraan")
private String tahun_kendaraan;
@Column(name = "merk")
private String merk;
@Column(name = "tambahan")
private String tambahan;
@Column(name = "kode_wilayah_plat")
private String kode_wilayah_plat;
@Column(name = "aksesoris_tambahan")
private String aksesoris_tambahan;
@Column(name = "harga_total")
private String harga_total;
@Column(name = "no_ref")
private String no_ref;
@Column(name = "tanggal")
private String tanggal;
@Column(name = "response")
private String response;
@Column(name = "response_message")
private String response_message;

@Lob
@Column(name = "foto_stnk")
private byte[] foto_stnk;
@Lob
@Column(name = "foto_mobil_depan")
private byte[] foto_mobil_depan;
@Lob
@Column(name = "foto_mobil_belakang")
private byte[] foto_mobil_belakang;
@Lob
@Column(name = "foto_mobil_kiri")
private byte[] foto_mobil_kiri;
@Lob
@Column(name = "foto_mobil_kanan")
private byte[] foto_mobil_kanan;
@Lob
@Column(name = "foto_dashtt")
private byte[] foto_dashtt;
@Lob
@Column(name = "foto_aksesoris")
private byte[] foto_aksesoris;
@Lob
@Column(name = "foto_no_rangka")
private byte[] foto_no_rangka;
@Lob
@Column(name = "foto_no_mesin")
private byte[] foto_no_mesin;



public Asken() {
}



public Asken(String nama_lengkap, String no_hp, String email, String no_plat, String no_mesin,
		String no_rangka, String sumber_rekening, String produk, String periode_asuransi, String tahun_kendaraan,
		String merk, String tambahan, String kode_wilayah_plat, String aksesoris_tambahan, String harga_total,
		String no_ref, String tanggal, String response, String response_message, byte[] foto_stnk,
		byte[] foto_mobil_depan, byte[] foto_mobil_belakang, byte[] foto_mobil_kiri, byte[] foto_mobil_kanan,
		byte[] foto_dashtt, byte[] foto_aksesoris, byte[] foto_no_rangka, byte[] foto_no_mesin) {
	super();
	
	this.nama_lengkap = nama_lengkap;
	this.no_hp = no_hp;
	this.email = email;
	this.no_plat = no_plat;
	this.no_mesin = no_mesin;
	this.no_rangka = no_rangka;
	this.sumber_rekening = sumber_rekening;
	this.produk = produk;
	this.periode_asuransi = periode_asuransi;
	this.tahun_kendaraan = tahun_kendaraan;
	this.merk = merk;
	this.tambahan = tambahan;
	this.kode_wilayah_plat = kode_wilayah_plat;
	this.aksesoris_tambahan = aksesoris_tambahan;
	this.harga_total = harga_total;
	this.no_ref = no_ref;
	this.tanggal = tanggal;
	this.response = response;
	this.response_message = response_message;
	this.foto_stnk = foto_stnk;
	this.foto_mobil_depan = foto_mobil_depan;
	this.foto_mobil_belakang = foto_mobil_belakang;
	this.foto_mobil_kiri = foto_mobil_kiri;
	this.foto_mobil_kanan = foto_mobil_kanan;
	this.foto_dashtt = foto_dashtt;
	this.foto_aksesoris = foto_aksesoris;
	this.foto_no_rangka = foto_no_rangka;
	this.foto_no_mesin = foto_no_mesin;
}



public String getId() {
	return id;
}



public void setId(String id) {
	this.id = id;
}






public String getNama_lengkap() {
	return nama_lengkap;
}



public void setNama_lengkap(String nama_lengkap) {
	this.nama_lengkap = nama_lengkap;
}



public String getNo_hp() {
	return no_hp;
}



public void setNo_hp(String no_hp) {
	this.no_hp = no_hp;
}



public String getEmail() {
	return email;
}



public void setEmail(String email) {
	this.email = email;
}



public String getNo_plat() {
	return no_plat;
}



public void setNo_plat(String no_plat) {
	this.no_plat = no_plat;
}



public String getNo_mesin() {
	return no_mesin;
}



public void setNo_mesin(String no_mesin) {
	this.no_mesin = no_mesin;
}



public String getNo_rangka() {
	return no_rangka;
}



public void setNo_rangka(String no_rangka) {
	this.no_rangka = no_rangka;
}



public String getSumber_rekening() {
	return sumber_rekening;
}



public void setSumber_rekening(String sumber_rekening) {
	this.sumber_rekening = sumber_rekening;
}



public String getProduk() {
	return produk;
}



public void setProduk(String produk) {
	this.produk = produk;
}



public String getPeriode_asuransi() {
	return periode_asuransi;
}



public void setPeriode_asuransi(String periode_asuransi) {
	this.periode_asuransi = periode_asuransi;
}



public String getTahun_kendaraan() {
	return tahun_kendaraan;
}



public void setTahun_kendaraan(String tahun_kendaraan) {
	this.tahun_kendaraan = tahun_kendaraan;
}



public String getMerk() {
	return merk;
}



public void setMerk(String merk) {
	this.merk = merk;
}



public String getTambahan() {
	return tambahan;
}



public void setTambahan(String tambahan) {
	this.tambahan = tambahan;
}



public String getKode_wilayah_plat() {
	return kode_wilayah_plat;
}



public void setKode_wilayah_plat(String kode_wilayah_plat) {
	this.kode_wilayah_plat = kode_wilayah_plat;
}



public String getAksesoris_tambahan() {
	return aksesoris_tambahan;
}



public void setAksesoris_tambahan(String aksesoris_tambahan) {
	this.aksesoris_tambahan = aksesoris_tambahan;
}



public String getHarga_total() {
	return harga_total;
}



public void setHarga_total(String harga_total) {
	this.harga_total = harga_total;
}



public String getNo_ref() {
	return no_ref;
}



public void setNo_ref(String no_ref) {
	this.no_ref = no_ref;
}



public String getTanggal() {
	return tanggal;
}



public void setTanggal(String tanggal) {
	this.tanggal = tanggal;
}



public String getResponse() {
	return response;
}



public void setResponse(String response) {
	this.response = response;
}



public String getResponse_message() {
	return response_message;
}



public void setResponse_message(String response_message) {
	this.response_message = response_message;
}



public byte[] getFoto_stnk() {
	return foto_stnk;
}



public void setFoto_stnk(byte[] foto_stnk) {
	this.foto_stnk = foto_stnk;
}



public byte[] getFoto_mobil_depan() {
	return foto_mobil_depan;
}



public void setFoto_mobil_depan(byte[] foto_mobil_depan) {
	this.foto_mobil_depan = foto_mobil_depan;
}



public byte[] getFoto_mobil_belakang() {
	return foto_mobil_belakang;
}



public void setFoto_mobil_belakang(byte[] foto_mobil_belakang) {
	this.foto_mobil_belakang = foto_mobil_belakang;
}



public byte[] getFoto_mobil_kiri() {
	return foto_mobil_kiri;
}



public void setFoto_mobil_kiri(byte[] foto_mobil_kiri) {
	this.foto_mobil_kiri = foto_mobil_kiri;
}



public byte[] getFoto_mobil_kanan() {
	return foto_mobil_kanan;
}



public void setFoto_mobil_kanan(byte[] foto_mobil_kanan) {
	this.foto_mobil_kanan = foto_mobil_kanan;
}



public byte[] getFoto_dashtt() {
	return foto_dashtt;
}



public void setFoto_dashtt(byte[] foto_dashtt) {
	this.foto_dashtt = foto_dashtt;
}



public byte[] getFoto_aksesoris() {
	return foto_aksesoris;
}



public void setFoto_aksesoris(byte[] foto_aksesoris) {
	this.foto_aksesoris = foto_aksesoris;
}



public byte[] getFoto_no_rangka() {
	return foto_no_rangka;
}



public void setFoto_no_rangka(byte[] foto_no_rangka) {
	this.foto_no_rangka = foto_no_rangka;
}



public byte[] getFoto_no_mesin() {
	return foto_no_mesin;
}



public void setFoto_no_mesin(byte[] foto_no_mesin) {
	this.foto_no_mesin = foto_no_mesin;
}



public static long getSerialversionuid() {
	return serialVersionUID;
}






}