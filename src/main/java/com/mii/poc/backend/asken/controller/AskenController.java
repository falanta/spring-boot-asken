 package com.mii.poc.backend.asken.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mii.poc.backend.asken.entity.Asken;
import com.mii.poc.backend.asken.entity.Informasi;
//import com.mii.poc.backend.asken.repository.AskenRepository;

@RestController
public class AskenController {
//	@Autowired
//	AskenRepository repository;
	
	@CrossOrigin
	@GetMapping("/cari")
	public String cari() {
// save a single Account
		
		return "{\r\n" + 
				"	\"asuransi_1\": \r\n" + 
				"		{\r\n" + 
				"			\"nama_produk\": \"Total Loss Only\",\r\n" + 
				"			\"merk\":\"Autocilin\",\r\n" + 
				"			\"konten_1\":{\r\n" + 
				"					\"judul\":\"Perlindungan Kerusakan Total\",\r\n" + 
				"					\"deskripsi\":\"Uang pertanggungan sampai dengan IDR 126.000.000\"\r\n" + 
				"					}\r\n" + 
				"			\"konten_2\":{\r\n" + 
				"					\"judul\":\"Fasilitas Derek Autocilin\",\r\n" + 
				"					\"deskripsi\":\"Layanan Mobil derek gratis dari Autocilin untuk kecelakaan kendaraan total IDR 554.400\"\r\n" + 
				"					}\r\n" + 
				"		 },\r\n" + 
				"	\"asuransi_2\":\r\n" + 
				"		{\r\n" + 
				"			\"nama_produk\": \"Comprehensive (All Risk)\",\r\n" + 
				"			\"merk\":\"Autocilin\",\r\n" + 
				"			\"konten_1\":{\r\n" + 
				"					\"judul\":\"Perlindungan segala Jenis Kerusakan\",\r\n" + 
				"					\"deskripsi\":\"Uang pertanggungan sampai dengan IDR 126.000.000\"\r\n" + 
				"					}\r\n" + 
				"			\"konten_2\":{\r\n" + 
				"					\"judul\":\"Bengkel Rekanan Terpercaya\",\r\n" + 
				"					\"deskripsi\":\"Jaminan keaslian suku cadang dan garansi 6 bulan setelah perbaikan\"\r\n" + 
				"					}\r\n" + 
				"			\"konten_3\":{\r\n" + 
				"					\"judul\":\"Emergency Roadside Assistance & Fasilitas Derek\",\r\n" + 
				"					\"deskripsi\":\"Layanan gratis untuk gangguan darurat, termasuk layanan derek untuk kecelakaan & kerusaan mesin\"\r\n" + 
				"					}\r\n" + 
				"			\"konten_4\":{\r\n" + 
				"					\"judul\":\"Same Day Repair\",\r\n" + 
				"					\"deskripsi\":\"Perbaikan di hari yang sama untuk kerusakan ringan pada bengkel rekanan terpilih\"\r\n" + 
				"					}\r\n" + 
				"		 }\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/detail_asuransi")
	public String detail() {
// save a single Account
		
		return "{\r\n" + 
				"	\"asuransi\":\"Total Loss Only\",\r\n" + 
				"	\"perusahaan\":\"Adira Insurance\",\r\n" + 
				"	\"manfaat\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua -Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. -Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. -Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.-Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.-Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.\",\r\n" + 
				"	\"syarat_ketentuan\":\"-Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. -Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. -Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. -Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\",\r\n" + 
				"	\"klaim\":\"-Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. -Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. -Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. -Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.-Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. -Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. -Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. -Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Premi 100.000/bulan\",\r\n" + 
				"	\"manfaat_foooter\":\"Informasi Produk & Daftar Bengkel Rekanan\",\r\n" + 
				"	\"syarat_ketentuan_footer\":\"Informasi Produk & Daftar Bengkel Rekanan\",\r\n" + 
				"	\"klaim_footer\":\r\n" + 
				"		{\r\n" + 
				"			\"telepon\":\"1500 456(24-hour Hotline)\",\r\n" + 
				"			\"email\":\"adiracare@asuransiadira.co.id\"\r\n" + 
				"		},\r\n" + 
				"	\"total_harga\":\"IDR 554.400/tahun\"\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/perluasan_perlindungan")
	public String perluasan_perlindungan() {
// save a single Account
		
		return "{\r\n" + 
				"	\"tipe_1\":\r\n" + 
				"		{\r\n" + 
				"			\"judul\":\"Angin Topan, Banjir, Badai, Hujan Es dan Tanah Longsor\",\r\n" + 
				"			\"harga\":\"IDR 63.500\",\r\n" + 
				"			\"deskripsi\":\"Jaminan ganti rugi atau biaya perbaikan terhadap kerusakan pada kendaraan yang disebabkan secara langsung oleh Angin Topan, Banjir, Badai, Hujan Es dan Tanah Longsor\"\r\n" + 
				"		},\r\n" + 
				"	\"tipe_2\":\r\n" + 
				"		{\r\n" + 
				"			\"judul\":\"Terorisme dan Sabotase\",\r\n" + 
				"			\"harga\":\"IDR 44.100\",\r\n" + 
				"			\"deskripsi\":\"Jaminan ganti rugi atau biaya perbaikan terhadap kerusakan pada kendaraan yang disebabkan secara langsung oleh Terorisme dan Sabotase\"\r\n" + 
				"		},\r\n" + 
				"	\"tipe_3\":\r\n" + 
				"		{\r\n" + 
				"			\"judul\":\"Huru Hara dan Kerusuhan\",\r\n" + 
				"			\"harga\":\"IDR 44.100\",\r\n" + 
				"			\"deskripsi\":\"Jaminan ganti rugi atau biaya perbaikan terhadap kerusakan pada kendaraan yang disebabkan secara langsung oleh Huru Hara dan Kerusuhan\"\r\n" + 
				"		}\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/informasi")
	public String informasi() {
// save a single Account
		
		return "{\r\n" + 
				"	\"no_referensi\": \"12345678901234001\",\r\n" + 
				"	\"tanggal\": \"16/12/2019 14:01\",\r\n" + 
				"	\"sumber_rekening\": \"tabungan Danamon Lebih 0035981199203 IDR\",\r\n" + 
				"	\"produk_asuransi\": \"Primajaga 100\",\r\n" + 
				"	\"uang_pertanggungan\": \"IDR 5.000.000,00\",\r\n" + 
				"	\"premi\": \"IDR 25.000,00\",\r\n" + 
				"	\"frekuensi_bayar\": \"bulanan\",\r\n" + 
				"	\"jangka_waktu_perlindungan\": \"10 tahun\",\r\n" + 
				"	\"masa_bayar_premi\": \"10 tahun\"\r\n" + 
				"}";
	}
	
	@CrossOrigin
	@GetMapping("/detail_status")
	public String detail_status() {
// save a single Account
		
		return "{\r\n" + 
				"\"status\": \"Berhasil/ Polis sudah terbit\",\r\n" + 
				"\"produk_asuransi\": \"Primajaga 100\",\r\n" + 
				"\"perusahaan\": \"Manulife\",\r\n" + 
				"\"no_polis\": \"1234567890123456\",\r\n" + 
				"\"uang_Pertanggungan\"=\"IDR 5.000.000,00\",\r\n" + 
				"\"premi\": \"IDR 25.000,00\",\r\n" + 
				"\"frekuensi_bayar\": \"Bulanan\",\r\n" + 
				"\"jangka_waktu_perlindungan\": \"10 tahun\",\r\n" + 
				"\"masa_bayar_premi\": \"10 tahun\"\r\n" + 
				"}";
	}
	
	@Autowired
    private KafkaTemplate<String, Asken> kafkaTemplate;
	private static final String TOPIC = "asken";
	
	@CrossOrigin
	@RequestMapping(value = "/kirim", method = RequestMethod.POST)
    public String kirim(
    		@RequestParam("nama_lengkap") String nama_lengkap,
    		@RequestParam("no_hp") String no_hp,
    		@RequestParam("email") String email,
    		@RequestParam("no_plat") String no_plat,
    		@RequestParam("no_mesin") String no_mesin,
    		@RequestParam("no_rangka") String no_rangka,
    		@RequestParam("foto_stnk") MultipartFile foto_stnk,
    		@RequestParam("foto_mobil_depan") MultipartFile foto_mobil_depan,
    		@RequestParam("foto_mobil_belakang") MultipartFile foto_mobil_belakang,
    		@RequestParam("foto_mobil_kiri") MultipartFile foto_mobil_kiri,
    		@RequestParam("foto_mobil_kanan") MultipartFile foto_mobil_kanan,
    		@RequestParam("foto_dashtt") MultipartFile foto_dashtt,
    		@RequestParam("foto_aksesoris") MultipartFile foto_aksesoris,
    		@RequestParam("foto_no_rangka") MultipartFile foto_no_rangka,
    		@RequestParam("foto_no_mesin") MultipartFile foto_no_mesin,
    		@RequestParam("sumber_rekening") String sumber_rekening,
    		@RequestParam("produk") String produk,
    		@RequestParam("periode_asuransi") String periode_asuransi,
    		@RequestParam("tahun_kendaraan") String tahun_kendaraan,
    		@RequestParam("merk") String merk,
    		@RequestParam("tambahan") String tambahan,
    		@RequestParam("kode_wilayah_plat") String kode_wilayah_plat,
    		@RequestParam("aksesoris_tambahan") String aksesoris_tambahan,
    		@RequestParam("harga_total") String harga_total
    		) throws Exception     
{
 
	
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String no_ref="123456789";
		String tanggal=formatter.format(date);
		String response="Pembelian sedang di proses.";
		String response_message="Selamat, Anda telah mengirimkan data anda sebagai untuk pemesanan Asuransi Mobil. Tim kami akan menghubungi Anda.";
		Informasi informasi=new Informasi(nama_lengkap, no_hp, email, no_plat, no_mesin,
				no_rangka, sumber_rekening, produk, periode_asuransi, tahun_kendaraan,
				merk, tambahan, kode_wilayah_plat, aksesoris_tambahan, harga_total,
				no_ref, tanggal,response,response_message) ;
		
		 ObjectMapper mapper = new ObjectMapper();
		 String jsonString = mapper.writeValueAsString(informasi);
		 
		 
		 byte[] foto_stnk_b=foto_stnk.getBytes();
		 byte[] foto_mobil_depan_b=foto_mobil_depan.getBytes();
		 byte[] foto_mobil_belakang_b=foto_mobil_belakang.getBytes();
		 byte[] foto_mobil_kiri_b=foto_mobil_kiri.getBytes();
		 byte[] foto_mobil_kanan_b=foto_mobil_kanan.getBytes();
		 byte[] foto_dashtt_b=foto_dashtt.getBytes();
		 byte[] foto_aksesoris_b=foto_aksesoris.getBytes();
		 byte[] foto_no_rangka_b=foto_no_rangka.getBytes();
		 byte[] foto_no_mesin_b=foto_no_mesin.getBytes();
		 
		 
		 Asken asken=new Asken(nama_lengkap, no_hp, email, no_plat, no_mesin,
					no_rangka, sumber_rekening, produk, periode_asuransi, tahun_kendaraan,
					merk, tambahan, kode_wilayah_plat, aksesoris_tambahan, harga_total,
					no_ref, tanggal, response, response_message, foto_stnk_b,
					foto_mobil_depan_b, foto_mobil_belakang_b, foto_mobil_kiri_b, foto_mobil_kanan_b,
					foto_dashtt_b, foto_aksesoris_b, foto_no_rangka_b, foto_no_mesin_b);
		
		 this.kafkaTemplate.send(TOPIC, asken);
		 
  return jsonString;
}
	
	@CrossOrigin
	@GetMapping("/review_order")
	public String review_order() {
// save a single Account
		
		return "{\r\n" + 
				"	\"tipe_1\":\r\n" + 
				"		{\r\n" + 
				"			\"judul\":\"Angin Topan, Banjir, Badai, Hujan Es dan Tanah Longsor\",\r\n" + 
				"			\"harga\":\"IDR 63.500\",\r\n" + 
				"			\"deskripsi\":\"Jaminan ganti rugi atau biaya perbaikan terhadap kerusakan pada kendaraan yang disebabkan secara langsung oleh Angin Topan, Banjir, Badai, Hujan Es dan Tanah Longsor\"\r\n" + 
				"		},\r\n" + 
				"	\"tipe_2\":\r\n" + 
				"		{\r\n" + 
				"			\"judul\":\"Terorisme dan Sabotase\",\r\n" + 
				"			\"harga\":\"IDR 44.100\",\r\n" + 
				"			\"deskripsi\":\"Jaminan ganti rugi atau biaya perbaikan terhadap kerusakan pada kendaraan yang disebabkan secara langsung oleh Terorisme dan Sabotase\"\r\n" + 
				"		},\r\n" + 
				"	\"tipe_3\":\r\n" + 
				"		{\r\n" + 
				"			\"judul\":\"Huru Hara dan Kerusuhan\",\r\n" + 
				"			\"harga\":\"IDR 44.100\",\r\n" + 
				"			\"deskripsi\":\"Jaminan ganti rugi atau biaya perbaikan terhadap kerusakan pada kendaraan yang disebabkan secara langsung oleh Huru Hara dan Kerusuhan\"\r\n" + 
				"		}\r\n" + 
				"}";
	}

}