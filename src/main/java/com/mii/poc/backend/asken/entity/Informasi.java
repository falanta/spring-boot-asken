package com.mii.poc.backend.asken.entity;

public class Informasi {

	String nama_lengkap;
	String no_hp;
	String email;
	 String alamat;
	String no_plat;
	String no_mesin;
	String no_rangka;
	String sumber_rekening;
	String produk;
	String periode_asuransi;
	String tahun_kendaraan;
	String merk;
	String tambahan;
	String kode_wilayah_plat;
	String aksesoris_tambahan;
	String harga_total;
	String no_ref="123456789";
	String tanggal;
	String response;
	String response_message;
	
	public Informasi() {
		
	}
	
	

	public Informasi(String nama_lengkap, String no_hp, String email, String no_plat, String no_mesin,
			String no_rangka, String sumber_rekening, String produk, String periode_asuransi, String tahun_kendaraan,
			String merk, String tambahan, String kode_wilayah_plat, String aksesoris_tambahan, String harga_total,
			String no_ref, String tanggal,String response,String response_message) {
		super();
		this.nama_lengkap = nama_lengkap;
		this.no_hp = no_hp;
		this.email = email;
		this.no_plat = no_plat;
		this.no_mesin = no_mesin;
		this.no_rangka = no_rangka;
		this.sumber_rekening = sumber_rekening;
		this.produk = produk;
		this.periode_asuransi = periode_asuransi;
		this.tahun_kendaraan = tahun_kendaraan;
		this.merk = merk;
		this.tambahan = tambahan;
		this.kode_wilayah_plat = kode_wilayah_plat;
		this.aksesoris_tambahan = aksesoris_tambahan;
		this.harga_total = harga_total;
		this.no_ref = no_ref;
		this.tanggal = tanggal;
		this.response=response;
		this.response_message=response_message;
	}

	

	public String getResponse() {
		return response;
	}



	public void setResponse(String response) {
		this.response = response;
	}



	public String getResponse_message() {
		return response_message;
	}



	public void setResponse_message(String response_message) {
		this.response_message = response_message;
	}



	public String getNama_lengkap() {
		return nama_lengkap;
	}

	public void setNama_lengkap(String nama_lengkap) {
		this.nama_lengkap = nama_lengkap;
	}

	public String getNo_hp() {
		return no_hp;
	}

	public void setNo_hp(String no_hp) {
		this.no_hp = no_hp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getNo_plat() {
		return no_plat;
	}

	public void setNo_plat(String no_plat) {
		this.no_plat = no_plat;
	}

	public String getNo_mesin() {
		return no_mesin;
	}

	public void setNo_mesin(String no_mesin) {
		this.no_mesin = no_mesin;
	}

	public String getNo_rangka() {
		return no_rangka;
	}

	public void setNo_rangka(String no_rangka) {
		this.no_rangka = no_rangka;
	}

	public String getSumber_rekening() {
		return sumber_rekening;
	}

	public void setSumber_rekening(String sumber_rekening) {
		this.sumber_rekening = sumber_rekening;
	}

	public String getProduk() {
		return produk;
	}

	public void setProduk(String produk) {
		this.produk = produk;
	}

	public String getPeriode_asuransi() {
		return periode_asuransi;
	}

	public void setPeriode_asuransi(String periode_asuransi) {
		this.periode_asuransi = periode_asuransi;
	}

	public String getTahun_kendaraan() {
		return tahun_kendaraan;
	}

	public void setTahun_kendaraan(String tahun_kendaraan) {
		this.tahun_kendaraan = tahun_kendaraan;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public String getTambahan() {
		return tambahan;
	}

	public void setTambahan(String tambahan) {
		this.tambahan = tambahan;
	}

	public String getKode_wilayah_plat() {
		return kode_wilayah_plat;
	}

	public void setKode_wilayah_plat(String kode_wilayah_plat) {
		this.kode_wilayah_plat = kode_wilayah_plat;
	}

	public String getAksesoris_tambahan() {
		return aksesoris_tambahan;
	}

	public void setAksesoris_tambahan(String aksesoris_tambahan) {
		this.aksesoris_tambahan = aksesoris_tambahan;
	}

	public String getHarga_total() {
		return harga_total;
	}

	public void setHarga_total(String harga_total) {
		this.harga_total = harga_total;
	}

	public String getNo_ref() {
		return no_ref;
	}

	public void setNo_ref(String no_ref) {
		this.no_ref = no_ref;
	}

	public String getTanggal() {
		return tanggal;
	}

	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	
	

	
	
	
	
	
}
